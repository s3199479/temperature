package nl.utwente.di.converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
        /* * * Tests t h e Quoter */
public class TestConverter {
    @Test
    public void testTemperature1() throws Exception {
        Converter converter = new Converter();
        double price = converter.convert(1);
        Assertions.assertEquals (33.8 , price , 0.0 , "Conversion of temp 1");
    }
}