package nl.utwente.di.converter;

public class Converter {
    double convert(int temp) {
        return (temp*1.8)+32;
    }
}
